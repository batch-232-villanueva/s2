package com.zuitt.example;

import java.util.Scanner;

public class ControlStructures {

    public static void main(String[] args) {
        /*
        Operators in Java
        Arithmetic - +, -, *, /, %
        Comparison - >, <, >=, <=, ==, !=
        Logical - &&, ||, !
        Assignment - =

        Conditional Structures in Java
        statement allows us to manipulate the flow of the code depending on the evaluation of the condition

        Syntax: if(condition){}
         */

        int num1 = 15;
        if (num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5");
        }

        // else statement will allow us to run a task or code if the condition fails or have a falsy values
        num1 = 36;
        if (num1 % 5 == 6) {
            System.out.println(num1 + " is divisible by 5");
        } else {
            System.out.println(num1 + " is not divisible by 5");
        }

        // Mini-Activity
        Scanner input = new Scanner(System.in);
//
//        System.out.println("Enter a number:");
//        int number = input.nextInt();
//
//        if(number % 2 == 0){
//            System.out.println(number + " is an even number");
//        } else {
//            System.out.println(number + " is an odd number");
//        }
//
        // short-circuiting
        int x = 15;
        int y = 0;
        if (y == 0 || x == 15 || y != 0) System.out.println(true);

        // ternary operator
        int num2 = 24;
        String result = (num2 > 0) ? Boolean.toString(true) : Boolean.toString(false);
        System.out.print(result);

        // Switch Cases
        // control flow structures that allow one code block to be run out of many other code block
        // this is often when the input is predictable

        System.out.println("Enter a number from 1-4:");
        int directionValue = input.nextInt();

        switch (directionValue){
            case 1:
                System.out.println("SM North EDSA");
                break;
            case 2:
                System.out.println("SM Southmall");
                break;
            case 3:
                System.out.println("SM City Taytay");
                break;
            case 4:
                System.out.println("SM Manila");
                break;
            default:
                System.out.println("Out of range");
                break;
        }
    }

}
